# Inkscape Beginner's Guide
# Copyright (C) 2018-2024, Inkscape Documentation Authors
# This file is distributed under the same license as the Inkscape Beginners'
# Guide package.
# Wen-Wei Kao <ltlnx@disroot.org>, 2024
#
msgid ""
msgstr ""
"Project-Id-Version: Inkscape Beginners' Guide 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-11 16:48+0800\n"
"PO-Revision-Date: 2024-01-06 05:37+0800\n"
"Last-Translator: Wen-Wei Kao <ltlnx@disroot.org>, 2024\n"
"Language: zh_TW\n"
"Language-Team: NCCU ODC Translation Team <nccuodc@gmail.com>\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.1\n"

#: ../../source/pencil-tool.rst:3
msgid "The Pencil Tool"
msgstr "鉛筆工具"

#: ../../source/pencil-tool.rst:5
msgid "|Icon for Pencil Tool| :kbd:`F6` or :kbd:`P`"
msgstr "|Icon for Pencil Tool| :kbd:`F6` 或 :kbd:`P`"

#: ../../source/pencil-tool.rst:152
msgid "Icon for Pencil Tool"
msgstr "鉛筆工具的圖示"

#: ../../source/pencil-tool.rst:7
msgid ""
"The behavior of the Pencil tool depends on the settings in its controls "
"bar. To draw with this tool, press the left mouse button and drag the "
"mouse around the canvas. The Pencil will leave a green trace that follows"
" the location of the mouse cursor. When you let go of the mouse button, "
"the shape you created will get its stroke (and/or its fill, if you have "
"one set)."
msgstr ""
"鉛筆工具的樣式會根據工具控制列的設定有所不同。如果要使用這個工具繪圖，"
"請按住滑鼠左鍵，並在畫布上拖曳。鉛筆工具會在滑鼠行經處留下一條綠色線條。"
"在您將滑鼠左鍵放開時，該形狀的邊框才會出現（如果有設定填充，也會出現）。"

#: ../../source/pencil-tool.rst:14
msgid ""
"Two tiny, square handles appear at the start and end of the drawn path. "
"When you start drawing on one of these handles, this will continue the "
"path, instead of creating a new object.  And if you stop drawing the same"
" path in one of those squares, it will close the path (meaning there are "
"no openings)."
msgstr ""
"兩個小正方形控制點會在路徑的頭尾兩端出現。如果您在其中一個控制點上繼續繪"
"畫，將會延續該路徑，而不會產生新的路徑。當您在其中一個正方形上放開左鍵、"
"結束繪畫，將會封閉該路徑（亦即路徑上沒有開口）。"

#: ../../source/pencil-tool.rst:20
msgid ""
"Let's have a look at the options of the Pencil tool. The "
":guilabel:`Shape` dropdown menu offers different brushes that influence "
"the shape of the path:"
msgstr ""
"我們接下來看看鉛筆工具的選項。:guilabel:`形狀` 下拉式選單提供了不同的筆"
"刷，用來影響路徑的形狀："

#: ../../source/pencil-tool.rst:26
msgid "Triangle in and out"
msgstr "內（外）三角"

#: ../../source/pencil-tool.rst:24
msgid ""
"This makes the path look a little more elegant, thinning or thickening "
"along its length. Switch to the Node tool and drag on the pink diamond-"
"shaped handle to adjust the width interactively."
msgstr ""
"這個選項可以沿著路徑調整線條粗細，讓路徑看起來更優雅。您可以切換到節點工"
"具，並拖曳粉色菱形控制點來調整線條寬度。"

#: ../../source/pencil-tool.rst:31
msgid "Ellipse"
msgstr "橢圓形"

#: ../../source/pencil-tool.rst:29
msgid ""
"The beginning and end of the path will be thinner than its middle part. "
"Switch to the Node tool and drag on the round white handle to adjust the "
"width interactively."
msgstr ""
"路徑的兩端會比中間的部分來得細。切換到節點工具，並拖曳白色菱形控制點來調整"
"寬度。"

#: ../../source/pencil-tool.rst:37
msgid "From clipboard"
msgstr "從剪貼簿"

#: ../../source/pencil-tool.rst:34
msgid ""
"You can make custom brushes by first drawing your brush shape, and then "
"copying it. This will add it to the clipboard automatically and can be "
"used as a brush for the pencil and pen tools."
msgstr ""
"您可以先畫出筆刷形狀、再複製該形狀，以自創筆刷。複製形狀之後選擇這個選項，"
"可以將剪貼簿內的形狀作為鉛筆工具與鋼筆工具的筆刷使用。"

#: ../../source/pencil-tool.rst:42
msgid "Bend from clipboard"
msgstr "從剪貼簿套用彎曲"

#: ../../source/pencil-tool.rst:40
msgid ""
"First you must copy a curve that already exists. The line that you draw "
"will be deformed like the path that you copied. And it will be "
"adjustable, using the Node tool."
msgstr ""
"首先您需要複製一個現有的路徑。您繪製的線條會套用所複製路徑的形變。使用節點"
"工具一樣可以調整效果。"

#: ../../source/pencil-tool.rst:48
msgid "Last applied"
msgstr "最近套用的"

#: ../../source/pencil-tool.rst:45
msgid ""
"Use the same shape you used last time. Use this if you want to continue "
"using a custom shape, for drawing multiple lines with the same style. "
"This way, you can go back to using the clipboard normally. It does no "
"longer have to hold the shape you want to draw."
msgstr ""
"使用與上次相同的形狀。如果想要繼續使用某個自訂筆刷來繪製更多條路徑，可以"
"使用這個選項。這樣一來，您就可以照常使用剪貼簿，而不用擔心形狀被蓋過去。"

#: ../../source/pencil-tool.rst:52
msgid "None"
msgstr "無"

#: ../../source/pencil-tool.rst:51
msgid ""
"The drawn path's outline will be of the same width along the whole length"
" of the path."
msgstr "繪製路徑的框線在整條路徑上都是同樣寬度。"

#: ../../source/pencil-tool.rst:54
msgid ""
"You can set the amount of :guilabel:`Smoothing` for the path you want to "
"draw. When you use a mouse for drawing, making this value larger will "
"make the line look less scrawly. When you use a graphics tablet with pen,"
" you can use a lower smoothing value, and it will still look nice."
msgstr ""
"您可以設定繪製的路徑的 :guilabel:`平滑` 程度。當您使用滑鼠或是觸控板作"
"畫，提高這個數值可以讓線條看起來不那麼粗糙。如果使用繪圖板繪畫，可以使用"
"較低的平滑值。"

#: ../../source/pencil-tool.rst:59
msgid ""
"The button |Icon for LPE based interactive simplify|:guilabel:`LPE based "
"interactive simplify` allows you to draw a path where you can adjust the "
"smoothing after you have finished drawing the path. Use the button |Icon "
"for LPE simplify flatten|:guilabel:`LPE simplify flatten` to lock the "
"result of the interactive smoothing. After it has been used, the path's "
"smoothing can no longer be adjusted."
msgstr ""
"|Icon for LPE based interactive simplify|:guilabel:`互動式簡化路徑特效` "
"按鈕可以讓您在繪製路徑後調整該路徑的平滑程度。使用 |Icon for LPE simplify "
"flatten|:guilabel:`LPE 簡化平坦` 則可以固定互動式平滑的結果。使用之後，該"
"路徑的平滑度將無法再做調整。"

#: ../../source/pencil-tool.rst:154
msgid "Icon for LPE based interactive simplify"
msgstr "互動式簡化路徑特效的圖示"

#: ../../source/pencil-tool.rst:156
msgid "Icon for LPE simplify flatten"
msgstr "LPE 簡化平坦的圖示"

#: ../../source/pencil-tool.rst:65
msgid ""
"'LPE' is the acronym for \"Live Path Effect\", a set of functionalities "
"specific to Inkscape that can change paths non-destructively, to achieve "
"spectacular results."
msgstr ""
"「LPE」是「路徑特效」（Live Path Effect）的縮寫。路徑特效是 Inkscape 獨有"
"的一套機制，能在不破壞路徑本身的情況下改變路徑的顯示方式，達到特殊的效果。"

#: ../../source/pencil-tool.rst:69
msgid ""
"To reset your changes to smoothing to the default value, you can use the "
"button where the hover text says :guilabel:`reset pencil parameters to "
"default`."
msgstr ""
"若要將平滑程度重設為預設值，可以使用工具提示為 :guilabel:`重設鉛筆參數為"
"預設值` 的按鈕。"

#: ../../source/pencil-tool.rst:72
msgid ""
"The pencil tool has three different **modes**. The results you get also "
"depend a lot on the level of smoothing:"
msgstr ""
"鉛筆工具有三個不同的**模式**。您繪製所得的結果與平滑的程度也大有關係："

#: ../../source/pencil-tool.rst:76
msgid "|Icon for Bezier path| Create regular Bézier path"
msgstr "|Icon for Bezier path| 建立規則的貝茲路徑"

#: ../../source/pencil-tool.rst:170
msgid "Icon for Bezier path"
msgstr "貝茲路徑的圖示"

#: ../../source/pencil-tool.rst:76
msgid ""
"The path that you get as a result is very close to the path that you drag"
" with the mouse cursor on the canvas. Remember to adjust the smoothing, "
"to make your line look more elegant."
msgstr ""
"使用此模式繪製的結果與實際在畫布上繪製的路徑相近。記得調整平滑值，讓線條"
"看起來更優雅。"

#: ../../source/pencil-tool.rst:79
msgid "|Icon for Spiro path| Create Spiro path"
msgstr "|Icon for Spiro path| 建立螺旋路徑"

#: ../../source/pencil-tool.rst:172
msgid "Icon for Spiro path"
msgstr "螺旋路徑的圖示"

#: ../../source/pencil-tool.rst:79
msgid "Create stylish swirls and curls with only the mouse!"
msgstr "只需使用滑鼠，就能畫出時尚的螺旋！"

#: ../../source/pencil-tool.rst:83
msgid "|Icon for BSpline path| Create BSpline path"
msgstr "|Icon for BSpline path| 建立貝氏雲形線路徑"

#: ../../source/pencil-tool.rst:174
msgid "Icon for BSpline path"
msgstr "貝氏雲形線路徑的圖示"

#: ../../source/pencil-tool.rst:82
msgid ""
"This mode reveals its use when you switch to the Node tool. It makes it "
"really easy to draw evenly smooth curves."
msgstr ""
"當您切換到節點工具時，就會看到這個模式的用處。使用這個模式，可以輕鬆繪製"
"均衡的平滑曲線。"

#: ../../source/pencil-tool.rst:85
msgid ""
"The Pencil tool (as well as the Pen tool) creates **dots** when you hold "
"down :kbd:`Ctrl` while clicking on the canvas. When you hold down both "
":kbd:`Shift` + :kbd:`Ctrl`, the dots' size will be doubled. With "
":kbd:`Ctrl` + :kbd:`Alt`, random sized small dots will be created with "
"every click, and with :kbd:`Shift` + :kbd:`Ctrl` + :kbd:`Alt`, each click"
" will generate a random sized big dot."
msgstr ""
"鉛筆工具（與鋼筆工具）在您按住 :kbd:`Ctrl` 並點擊畫布的時候，會產生**圓點**。"
"當您同時按住 :kbd:`Shift` + :kbd:`Ctrl`，圓點的大小會是原本的兩倍。使用 "
":kbd:`Ctrl` + :kbd:`Alt` 時，會在點擊處產生隨機大小的點，而使用 :kbd:`Shift` "
"+ :kbd:`Ctrl` + :kbd:`Alt` 會在點擊處產生尺寸更大的隨機大小圓點。"

#: ../../source/pencil-tool.rst:92
msgid "Note that the dots are really circles."
msgstr "這些圓點其實就是圓圈。"

#: ../../source/pencil-tool.rst:94
msgid "|Dots created with the pencil tool|"
msgstr "|Dots created with the pencil tool|"

#: ../../source/pencil-tool.rst:158
msgid "Dots created with the pencil tool"
msgstr "使用鉛筆工具產生的圓點。"

#: ../../source/pencil-tool.rst:96
msgid ""
"Dots created with the pencil tool. Top left: with :kbd:`Ctrl`, top right:"
" with :kbd:`Ctrl` + :kbd:`Alt`, bottom left: with :kbd:`Ctrl` + "
":kbd:`Shift`, bottom right: with :kbd:`Ctrl` + :kbd:`Alt` + Shift."
msgstr ""
"使用鉛筆工具產生的圓點。左上：使用 :kbd:`Ctrl`，右上：使用 :kbd:`Ctrl` + "
":kbd:`Alt`，左下：使用 :kbd:`Ctrl` + :kbd:`Shift`，右下：使用 :kbd:`Ctrl`"
" + :kbd:`Alt` + :kbd:`Shift`。"

#: ../../source/pencil-tool.rst:100
msgid "|A path drawn with Shape: Ellipse and no smoothing|"
msgstr "|A path drawn with Shape: Ellipse and no smoothing|"

#: ../../source/pencil-tool.rst:159
msgid "A path drawn with Shape: Ellipse and no smoothing"
msgstr "使用形狀為圓形，未使用平滑功能繪製的路徑"

#: ../../source/pencil-tool.rst:102
msgid "A path drawn with Shape: Ellipse and no smoothing."
msgstr "使用形狀為圓形，未使用平滑功能繪製的路徑。"

#: ../../source/pencil-tool.rst:104
msgid "The path has been extended from the square handle at its end.|"
msgstr "此路徑經由尾端的方形控制點延伸。|"

#: ../../source/pencil-tool.rst:106 ../../source/pencil-tool.rst:160
msgid "The path has been extended from the square handle at its end."
msgstr "此路徑經由尾端的方形控制點延伸。"

#: ../../source/pencil-tool.rst:108
msgid "|Shape: Ellipse with more smoothing|"
msgstr "|Shape: Ellipse with more smoothing|"

#: ../../source/pencil-tool.rst:161
msgid "Shape: Ellipse with more smoothing"
msgstr "形狀：圓形，平滑數值增加"

#: ../../source/pencil-tool.rst:110
msgid "Shape: Ellipse with more smoothing."
msgstr "形狀：圓形，平滑數值增加。"

#: ../../source/pencil-tool.rst:112
msgid "|Shape: Triangle Out was used here.|"
msgstr "|Shape: Triangle Out was used here.|"

#: ../../source/pencil-tool.rst:162
msgid "Shape: Triangle Out was used here."
msgstr "這裡使用形狀：外三角。"

#: ../../source/pencil-tool.rst:114
msgid "Shape: Triangle out was used here."
msgstr "這裡使用形狀：外三角。"

#: ../../source/pencil-tool.rst:116
msgid "|Path with Shape: From Clipboard|"
msgstr "|Path with Shape: From Clipboard|"

#: ../../source/pencil-tool.rst:163
msgid "Path with Shape: From Clipboard"
msgstr "這個路徑使用形狀：從剪貼簿"

#: ../../source/pencil-tool.rst:118
msgid "This path uses Shape: From Clipboard"
msgstr "這個路徑使用形狀：從剪貼簿"

#: ../../source/pencil-tool.rst:120
msgid "|This path was copied to the clipboard.|"
msgstr "|This path was copied to the clipboard.|"

#: ../../source/pencil-tool.rst:164
msgid "This path was copied to the clipboard."
msgstr "這是被複製的圖形。"

#: ../../source/pencil-tool.rst:122
msgid "The path that was copied to the clipboard for drawing the above path."
msgstr "這是被複製的圖形，用來繪製上方路徑。"

#: ../../source/pencil-tool.rst:124
msgid "|Path with Shape: Triangle In, Bézier mode, smoothing set to 40|"
msgstr "|Path with Shape: Triangle In, Bézier mode, smoothing set to 40|"

#: ../../source/pencil-tool.rst:165
msgid "Path with Shape: Triangle In, Bézier mode, smoothing set to 40"
msgstr "使用形狀：內三角繪製的路徑，平滑數值為 40"

#: ../../source/pencil-tool.rst:126
msgid "Path drawn with Shape: Triangle In in Bézier mode, with a smoothing of 40"
msgstr "使用形狀：內三角繪製的路徑，平滑數值為 40"

#: ../../source/pencil-tool.rst:129
msgid "|Path drawn in B-Spline mode|"
msgstr "|Path drawn in B-Spline mode|"

#: ../../source/pencil-tool.rst:131 ../../source/pencil-tool.rst:166
msgid "Path drawn in B-Spline mode"
msgstr "使用貝氏雲形線繪製的路徑"

#: ../../source/pencil-tool.rst:133
msgid "|Path drawn in Spiro mode with Shape: From clipboard|"
msgstr "|Path drawn in Spiro mode with Shape: From clipboard|"

#: ../../source/pencil-tool.rst:167
msgid "Path drawn in Spiro mode with Shape: From clipboard"
msgstr "使用形狀：從剪貼簿、在螺旋模式下繪製的路徑"

#: ../../source/pencil-tool.rst:135
msgid ""
"Path with Shape: From Clipboard drawn in Spiro mode with a smoothing "
"level of 40"
msgstr "使用形狀：從剪貼簿、在螺旋模式下繪製的路徑，平滑數值為 40"

#: ../../source/pencil-tool.rst:138
msgid "|The width of the path can be changed with the white circular handle.|"
msgstr "|The width of the path can be changed with the white circular handle.|"

#: ../../source/pencil-tool.rst:168
msgid "The width of the path can be changed with the white circular handle."
msgstr "路徑可以使用白色圓形控制點調整寬度。"

#: ../../source/pencil-tool.rst:140
msgid ""
"The same path as above, only wider. The width of a path that uses objects"
" on the clipboard (and those that use 'Shape: Ellipse', both use the "
"'Pattern along Path' Live Path Effect) can be changed with the Node tool,"
" using the white circular handle at the beginning of the path."
msgstr ""
"與上方相同，但更寬的路徑。使用剪貼簿物件繪製的路徑可以在節點工具下用白色"
"圓形控制點調整寬度。（使用形狀：圓形亦同，兩個形狀都是使用「圖樣沿置路徑」"
"LPE）"

#: ../../source/pencil-tool.rst:145
msgid ""
"|The width of the path can be changed with the pink diamond-shaped "
"handle.|"
msgstr ""
"|The width of the path can be changed with the pink diamond-shaped "
"handle.|"

#: ../../source/pencil-tool.rst:169
msgid "The width of the path can be changed with the pink diamond-shaped handle."
msgstr "路徑寬度可以使用粉色菱形節點調整"

#: ../../source/pencil-tool.rst:148
msgid ""
"The width of a path with a PowerStroke Live Path Effect (used by 'Shape: "
"Triangle In / Out') can be adjusted with the node tool by moving a pink, "
"diamond-shaped node, to achieve a non-constant path width."
msgstr ""
"使用「神奇邊框」即時路徑特效的路徑（像是使用形狀：內 / 外三角），寬度可以"
"在路徑工具下，使用粉色菱形節點調整，以做出不一致的寬度效果。"

