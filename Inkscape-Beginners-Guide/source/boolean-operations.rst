******************
Boolean Operations
******************

The Boolean Operations work on paths, or they try to convert the selected
objects to paths before they compute the result. All these operations need at
least 2 convertible objects or paths, which will be combined following specific
rules.

|Union icon| Union
  Keeps the common outline of all selected
  paths.

|Difference icon| Difference
  Subtracts one path from another one.

|Intersection icon| Intersection
  Only keeps those parts that are
  covered by all selected paths.

|Exclusion icon| Exclusion
  Keeps those parts which are covered by an odd number of paths (if you have
  two objects, this is where the objects do not overlap).

|Division icon| Division
  The path below is cut into pieces by the path above.

|Cut path icon| Cut Path
  Creates as many paths as there are path intersections between the two paths.

|Combine icon| Combine
  Keeps all parts, and combines them into a single object.

|Break apart icon| Break Apart
  If a path consists of a number of independent parts (subpaths), this will
  create that number of separate objects.

To use these operations, select the two (or more) objects, and then select the
option of your choice in the :guilabel:`Path` menu. The result will immediately
appear on the canvas - if it doesn't, read the error message that will appear in
the status bar, at the bottom of the Inkscape window, to find out about the
reason for the failure.

.. Hint:: The :doc:`stacking order <stacking-order>` of the object
          matters, so check that the bottom object is the one you want
          to apply the operation to. To know how each operation will
          apply, look at the icons: the circle is always on top of the
          square.

.. figure:: images/boolean_union.png
   :alt: Unioning a triangle and a square results in a house
   :class: screenshot

   Unioning a triangle and a square gives a house.

.. figure:: images/boolean_difference.png
   :alt: The rectangle will become the door opening after Difference
   :class: screenshot

   Difference between a rectangle and a house creates an opening for the door.

.. figure:: images/boolean_intersection_exclusion.png
   :alt: Two overlapping ellipses and the results of Intersection and Exclusion
   :class: screenshot

   Two overlapping ellipses. On the left: Intersection between the two ellipses. On the right: Exclusion between the two ellipses.

.. figure:: images/boolean_division.png
   :alt: Ellipse with a path on top. Ellipse divided by the path.
   :class: screenshot

   Someone has drawn a path with the pencil tool on the orange ellipse on the left. On the right: Division.

.. figure:: images/boolean_combine_break_apart.png
   :alt: The two parts of the ellipse have been combined into a single path (with subpaths). Each subpath has become a single path after Break Apart.
   :class: screenshot

   On the left: Move apart and combine (to form a single path composed of two subpaths). On the right: :guilabel:`Break Apart` separates all subpaths into independent objects.

.. |Union icon| image:: images/icons/path-union.*
   :class: inline
.. |Difference icon| image:: images/icons/path-difference.*
   :class: inline
.. |Intersection icon| image:: images/icons/path-intersection.*
   :class: inline
.. |Exclusion icon| image:: images/icons/path-exclusion.*
   :class: inline
.. |Division icon| image:: images/icons/path-division.*
   :class: inline
.. |Cut path icon| image:: images/icons/path-cut.*
   :class: inline
.. |Combine icon| image:: images/icons/path-combine.*
   :class: inline
.. |Break apart icon| image:: images/icons/path-break-apart.*
   :class: inline
